from __future__ import annotations

__authors__ = "Prof"
__version__ = "1.0.0"
__date__ = "2021/01"


class Animal:
    """Classe Animal
    Permet de créer une instance d'Animal

    Attributs
    ----------
    nom : str
        nom de l'animal
    tailleMoy : int
        taille moyenne de l'animal exprimée en millimetre (mm)
    """


    def __init__(self, unNom: str, unNomSci: str, taille: int, uneVie: int, unPoids: int):
        """Constructeur de la classe
        :param unNom : pour valoriser le nom de l'animal
        :param taille : pour valoriser la taille de l'animal
        """
        self.setNom(unNom)
        self.setNomSci(unNomSci)
        self.setTailleMoy(taille)
        self.setVie(uneVie)
        self.setPoids(unPoids)

    def getNomSci(self) -> str:
        return self.__nomSci

    def setNomSci(self, valeur: str) -> None:
        self.__nomSci = valeur

    def getNom(self) -> str:
        """Getter sur l'attribut nom
        :return:
        """
        return self.__nom

    def setNom(self, valeur: str) -> None:
        """Setter sur l'attribut nom. Permet sa valorisation
        :param valeur: nom
        """
        self.__nom = valeur

    def getTailleMoy(self) -> int:
        """Getter sur l'attribut tailleMoy
        :return:
        """
        return self.__tailleMoy

    def setTailleMoy(self, valeur: int) -> None:
        """Setter sur l'attribut tailleMoy. Permet sa valorisation
        :param valeur : la taille moyenne
        """
        self.__tailleMoy = valeur

    def comparerTaille(self, other: Animal) -> bool:
        """ Permet de comparer l'instance courante de classe avec un de ses semblables
        :param other: l'autre animal objet de la comparaison
        :return: True si l'instance courante est plus petite de par la taille que l'instance other. False sinon
        """
        return self.__tailleMoy <= other.__tailleMoy

    def comparerVie(self, other: Animal) -> bool:
        return self.__vie <= other.__vie

    def comparerPoids(self, other: Animal) -> bool:
        return self.__poids <= other.__poids

    def setVie(self, valeur: int) -> None:
        self.__vie = valeur

    def getVie(self) -> int:
        return self.__vie

    def setPoids(self, valeur: int) -> None:
        self.__poids = valeur

    def getPoids(self) -> int:
        return self.__poids

    def __str__(self) -> str:
        """Redéfinit le comportement avec print()
        :return: affiche le nom

        """
        return self.__nom + " (" + self.__nomSci + ")"
