from __future__ import annotations
from animal import Animal


class Joueur:
    __num: int

    def __init__(self, unNom: str, unNum: int, unDeck: list):
        self.setNom(unNom)
        self.setNum(unNum)
        self.setDeck(unDeck)

    def getNom(self) -> str:
        """Getter sur l'attribut nom
        :return:
        """
        return self.__nom

    def setNom(self, valeur: str) -> None:
        """Setter sur l'attribut nom. Permet sa valorisation
        :param valeur: nom
        """
        self.__nom = valeur

    def getNum(self) -> int:
        """Getter sur l'attribut nom
        :return:
        """
        return self.__num

    def setNum(self, valeur: int) -> None:
        """Setter sur l'attribut nom. Permet sa valorisation
        :param valeur: nom
        """
        self.__num = valeur

    def getDeck(self) -> list[Animal]:
        return self.__deck

    def setDeck(self, deck: list) -> None:
        self.__deck = deck

    def supprimerCarteDeck(self, uneCarte: Animal) -> None:
        self.__deck.remove(uneCarte)



    def __str__(self) -> str:
        return "Joueur n°" + str(self.__num) + " : " + str(self.__nom)
