# Programme s'inspirant du jeu Cardline
__authors__ = "Prof+le phasme trop bg 2 la street"
__version__ = "1.0.0"
__date__ = "2021/01"

from typing import List

from animal import Animal
from fonctions import afficherListe, getElemPrecedent, getElemSuivant, getElemInListeAleatoire, construireDeck, \
    creationPioche
from joueur import Joueur

# -------------------------------------------------------------------------------------------
# fonction d'affichage et de comparaison
# -------------------------------------------------------------------------------------------
# création des cartes
listeAnimaux = creationPioche()


def creationJoueurs(listeAnimaux):
    nombreDeJoueur = int(input("Nombre de joueur? "))
    i = 1
    cbCarte = int(input("Combien de carte par deck ? "))
    joueurs = []
    while i <= nombreDeJoueur:
        quelleJoueur = input("Nom du joueur n°" + str(i) + "? ")
        deck = construireDeck(cbCarte, listeAnimaux)
        joueurs.append(Joueur(quelleJoueur, i, deck))
        i += 1
    return joueurs


joueurs = creationJoueurs(listeAnimaux)


def menu():
    print("|--------------------------Joueurs-----------------------------|")
    for joueur in joueurs:
        print(joueur)
        print("Deck de " + joueur.getNom() + " :")
        afficherListe(joueur.getDeck())
    print("|----------------------------Menu------------------------------|")
    print("1 - afficher la liste des animaux")
    print("2 - jouer à la classification selon la taille ")
    print("3 - jouer à la classification selon la durée de vie ")
    print("4 - jouer à la classification selon le poids ")
    print("9 - quitter")
    choix = int(input("Votre Choix ? : "))
    while choix != 1 and choix != 2 and choix != 3 and choix != 4 and choix != 9:
        choix = int(input("Votre Choix ? : "))
    return choix


def jouerNouvelleCarte() -> str:
    """Permet de proposer de jouer une nouvelle carte O/N
    :return: la réponse : O ou N
    """
    choix = input("Jouer une nouvelle carte : O/N ?")
    while choix != 'O' and choix != 'N':
        choix = input("Jouer une nouvelle carte : O/N ?")
    return choix


def choisirPlace(numMax: int) -> int:
    """Permet de choisir la position pour placer la carte en respectant la taille de la liste.
    :param numMax : valeur max de placement possible
    :return: la position choisie
    """
    choixPlace = int(input("Place ? (Choisissez la place de l'animal qui sera situé juste après ou ajouter 1 à la "
                           "dernière position pour placer en fin de liste): "))
    while choixPlace < 0 or choixPlace > numMax:
        choixPlace = int(input("Place ? (Choisissez la place de l'animal qui sera situé juste après ou ajouter 1 à la "
                               "dernière position pour placer en fin de liste): "))
    return choixPlace


def comparer(ani: Animal, autreAni: Animal) -> bool:
    """Compare ani et autreAni selon la taille.
    :param ani : animal à comparer au second
    :param autreAni : animal à comparer
    :return : Renvoie True si la taille d'ani est plus petite ou égale à celle d'autreAni. False sinon
    """
    return ani.getTailleMoy() <= autreAni.getTailleMoy()


def testerInsertionAnimal(listeAni: List[Animal], pos: int, unAni: Animal, val: str) -> bool:
    """Permet de vérifier la position pour l'insertion dans une liste
    :param listeAni: liste des animaux ordonnées selon la taille
    :param pos: place proposée pour insérer
    :param unAni: animal
    :return:
        Renvoie True si place est la bonne position pour insérer unAni dans la liste en respectant le critère de taille. False sinon.
    """
    animalPrec = getElemPrecedent(listeAni, pos)
    animalSuiv = getElemSuivant(listeAni, pos)
    if val == "t":

        ok = False
        if animalPrec:
            if animalSuiv:
                if animalPrec.comparerTaille(unAni) and unAni.comparerTaille(animalSuiv):
                    ok = True
            else:
                # ajout en fin de liste : suiv n'existe pas
                if animalPrec.comparerTaille(unAni):
                    ok = True
        else:
            # ajout en début de liste : prec n'existe pas
            if animalSuiv:
                if unAni.comparerTaille(animalSuiv):
                    ok = True
        return ok
    elif val == "v":
        ok = False
        if animalPrec:
            if animalSuiv:
                if animalPrec.comparerVie(unAni) and unAni.comparerVie(animalSuiv):
                    ok = True
            else:
                # ajout en fin de liste : suiv n'existe pas
                if animalPrec.comparerVie(unAni):
                    ok = True
        else:
            # ajout en début de liste : prec n'existe pas
            if animalSuiv:
                if unAni.comparerVie(animalSuiv):
                    ok = True
        return ok
    elif val == "c":
        ok = False
        if animalPrec:
            if animalSuiv:
                if animalPrec.comparerPoids(unAni) and unAni.comparerPoids(animalSuiv):
                    ok = True
            else:
                # ajout en fin de liste : suiv n'existe pas
                if animalPrec.comparerPoids(unAni):
                    ok = True
        else:
            # ajout en début de liste : prec n'existe pas
            if animalSuiv:
                if unAni.comparerPoids(animalSuiv):
                    ok = True
        return ok


# ----------------------------------------------------------------------------------------------------------------#
# PROGRAMME PRINCIPAL -------------------------------------------------------------------------------------------#
# ----------------------------------------------------------------------------------------------------------------#


# initialisation de la liste des animaux de la partie
listeAnimauxPartie = []

print(
    "Bienvenue dans le jeu Cardline : le but consiste à ordonnner les animaux selon une caractéristique : la taille ou l'espérence de vie")

choix = menu()
nbCarte = 0

while choix != 9:

    if choix == 1:
        print("|-------------------------Cardline ----------------------------|")
        afficherListe(listeAnimaux)
    elif choix == 3 or 2 or 4:
        if choix == 3:
            val = "v"
        elif choix == 2:
            val = "t"
        elif choix == 4:
            val = "c"

        # initialisation de la partie : tirage au sort d'un premier animal
        listeAnimauxPartie.append(getElemInListeAleatoire(listeAnimaux))
        for tourjoue in range(len(joueurs)):
            while len(joueurs[tourjoue].getDeck()) != 0 and nbCarte != len(
                    listeAnimaux) - 1 and jouerNouvelleCarte() == 'O':  # on peut jouer tant que tous les animaux n'ont pas été placés
                for tourjoue in range(len(joueurs)):
                    print("\n--------- Cardline ---------")
                    afficherListe(listeAnimauxPartie)
                    print("----------------------------")

                    # tirage au sort d'un animal non encore joué
                    print("C'est au tour de " + joueurs[tourjoue].getNom() + " ,son deck est:")
                    afficherListe(joueurs[tourjoue].getDeck())
                    choixAni = int(input("Quelle est l'index de l'animal que tu joues ?"))
                    unAnimal = (joueurs[tourjoue].getDeck())[choixAni]
                    while unAnimal in listeAnimauxPartie:
                        unAnimal = (joueurs[tourjoue].getDeck())
                    print("Nouvel animal à placer : " + str(unAnimal) + "\n")

                    # Choix de la place par le joueur
                    place = choisirPlace(len(listeAnimauxPartie))

                    # vérification de la proposition du joueur
                    if testerInsertionAnimal(listeAnimauxPartie, place, unAnimal, val):
                        print("Bravo !")
                        listeAnimauxPartie.insert(place, unAnimal)
                        joueurs[tourjoue].supprimerCarteDeck(unAnimal)
                    else:
                        print("Perdu :(")
                        joueurs[tourjoue].supprimerCarteDeck(unAnimal)
                        listeAnimaux.append(unAnimal)
                        nouvCarte = getElemInListeAleatoire(listeAnimaux)
                        joueurs[tourjoue].getDeck().append(nouvCarte)
                        listeAnimaux.remove(nouvCarte)

                    print("\n--------------- Cardline ---------------")
                    afficherListe(listeAnimauxPartie)
                    print("------------------------------------------")
                    if len(joueurs[tourjoue].getDeck()) == 0:
                        print("Bravo tu as gagnée " + joueurs[tourjoue].getNom() + " ton deck est vide !!!")

    choix = menu()
