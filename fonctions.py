# fonctions permettant la manipulation d'une liste d'objets
import csv
import random
from typing import List
from animal import Animal
from joueur import Joueur


def afficherListe(liste: List[Animal]) -> None:
    """
    Permet d'afficher le contenu d'une liste
    :param liste: liste d'objets
    """
    for i in range(len(liste)):
        print(str(i) + " - " + str(liste[i]))


def getElemPrecedent(liste: List[Animal], pos: int) -> Animal:
    """
    Retourne l'element précédent la position donnée. None s'il n'existe pas (début de liste)
    :param liste: liste d'objets
    :param pos: position
    """
    prec = None
    if pos > 0:
        prec = liste[pos - 1]
    return prec


def getElemSuivant(liste: List[Animal], pos: int) -> Animal:
    """
    Retourne l'élement suivant la position donnée. None s'il n'existe pas (fin de liste)
    :param liste: liste d'objets
    :param pos : entier
    """
    suiv = None
    if pos <= len(liste) - 1:
        suiv = liste[pos]
    return suiv


def getElemInListeAleatoire(liste: object) -> object:
    """
    Renvoie un element tiré au sort dans la liste passée en paramètre
    :param liste: liste d'objets

    :rtype: Animal
    """
    elem = liste[random.randint(0, len(liste) - 1)]
    return elem


def construireDeck(nbCartes: int, pioche: List[Animal]):
    deck = []
    for i in range (nbCartes):
        carte = getElemInListeAleatoire(pioche)
        deck.append(carte)
        pioche.remove(carte)
    return deck


def creationPioche() -> List[Animal]:
    with open('tableauAnimaux.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        pioche = []
        for row in spamreader:
            pioche.append(Animal(row[0], row[1], int(row[2]), int(row[3]), int(row[4])))
    return pioche
